package date_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/date"
)

func TestToISODate(t *testing.T) {
	t.Run("should return error because of invalid location", func(t *testing.T) {
		loc := "test-unknown"
		time := time.Now()
		_, err := date.ToISODate(time, loc)
		assert.Error(t, err)
	})

	t.Run("should return success", func(t *testing.T) {
		expected := "2019-12-31T17:00:00.000Z"
		loc := "UTC"
		time, _ := time.Parse(date.ISOFormat, expected)
		isoString, _ := date.ToISODate(time, loc)
		assert.Equal(t, expected, isoString)
	})
}

func TestToMySQLDate(t *testing.T) {
	t.Run("should return error because of invalid location", func(t *testing.T) {
		loc := "test-unknown"
		time := time.Now()
		_, err := date.ToMySQLDate(time, loc)
		assert.Error(t, err)
	})

	t.Run("should return success", func(t *testing.T) {
		expected := "2006-01-02 15:04:05.000"
		loc := "UTC"
		time, _ := time.Parse(date.MySQLFormat, expected)
		isoString, _ := date.ToMySQLDate(time, loc)
		assert.Equal(t, expected, isoString)
	})
}
