package wrapper

// Status is a type that describe the request
type Status string

// StatOK will describe that the status is currently ok
var StatOK Status = "OK"

// StatInsufficientBalance describe that the request is forbidden due to insufficient balance
var StatInsufficientBalance Status = "INSUFFICIENT_BALANCE"

// StatInvalidUser describe that the request is forbidden due to invalid user
var StatInvalidUser Status = "INVALID_USER"

// StatInvalidRequest describe that the request is rejected due to invalid parameters, body, or query string
var StatInvalidRequest Status = "INVALID_REQUEST"

// StatUnexpectedError describe that the request is rejected due to invalid parameters, body, or query string
var StatUnexpectedError Status = "UNEXPECTED_ERROR"

// StatNotModified describe the the update request is success but no one document that is modified after action
var StatNotModified Status = "NOT_MODIFIED"
