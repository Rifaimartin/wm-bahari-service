module gitlab.com/Rifaimartin/wm-bahari-service

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/google/uuid v1.1.2
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	go.elastic.co/apm/module/apmgorilla v1.8.0
	go.elastic.co/apm/module/apmlogrus v1.8.0
	go.elastic.co/apm/module/apmsql v1.8.0
)
