package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path"
	"runtime"
	"strings"
	"time"

	gh "github.com/gorilla/handlers"

	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/date"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/middleware"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/service"

	"github.com/gorilla/mux"
	_ "github.com/joho/godotenv/autoload" // for development
	"github.com/sirupsen/logrus"
	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/wrapper"
	"go.elastic.co/apm/module/apmlogrus"
	"go.elastic.co/apm/module/apmsql"
	_ "go.elastic.co/apm/module/apmsql/mysql"
)

func main() {
	appName := os.Getenv("APP_NAME")
	port := os.Getenv("PORT")
	mariadbHost := os.Getenv("MARIADB_HOST")
	mariadbPort := os.Getenv("MARIADB_PORT")
	mariadbUser := os.Getenv("MARIADB_USER")
	mariadbPassword := os.Getenv("MARIADB_PASSWORD")
	mariadbDBName := os.Getenv("MARIADB_DATABASE")
	basicAuthUsername := os.Getenv("BASIC_AUTH_USERNAME")
	basicAuthPassword := os.Getenv("BASIC_AUTH_PASSWORD")

	// set logger with apm hook
	logger := logrus.New()
	logger.SetFormatter(getLogFormatter())
	logger.SetReportCaller(true)
	logger.AddHook(&apmlogrus.Hook{
		LogLevels: logrus.AllLevels,
	})

	// // set basic auth
	basicAuth := &middleware.BasicAuth{
		Username: basicAuthUsername,
		Password: basicAuthPassword,
	}

	// set mongodb
	// mc, err := mongodb.NewClient(mongodbURL)
	// if err != nil {
	// 	logger.Fatal(err)
	// }
	// if err = mc.Connect(context.Background()); err != nil {
	// 	logger.Fatal(err)
	// }
	// mdb := mc.Database()

	// set mariadb
	mariadbConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", mariadbUser, mariadbPassword, mariadbHost, mariadbPort, mariadbDBName)
	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")
	dsn := fmt.Sprintf("%s?%s", mariadbConnectionString, val.Encode())
	mariadbDB, err := apmsql.Open("mysql", dsn)
	if err != nil {
		logger.Fatal(err)
	}
	err = mariadbDB.Ping()
	if err != nil {
		logger.Fatal(err)
	}
	mariadbDB.SetMaxOpenConns(100)
	mariadbDB.SetMaxIdleConns(5)
	mariadbDB.SetConnMaxLifetime(time.Minute * 5)

	// set router
	router := mux.NewRouter()

	// set router default headers
	router.Use(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Server", appName)
			next.ServeHTTP(w, r)
		})
	})

	// set router not found handler
	router.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		result := new(wrapper.Result)
		result.Err = wrapper.ErrNotFound
		result.ErrMessage = "Resource is not found"
		result.Status = wrapper.StatInvalidRequest

		wrapper.ResponseError(w, result)
	})

	// set router method not allowed handler
	router.MethodNotAllowedHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		result := new(wrapper.Result)
		result.Err = wrapper.ErrMethodNotAllowed
		result.ErrMessage = "Method is not allowed"
		result.Status = wrapper.StatInvalidRequest

		wrapper.ResponseError(w, result)
	})

	// set cors handler
	handler := gh.CORS(
		gh.AllowedOrigins([]string{"*"}),
		gh.AllowedHeaders([]string{"X-Requested-With", "Origin", "Content-Type", "Authorization"}),
		gh.AllowedMethods([]string{http.MethodPost, http.MethodGet, http.MethodPut, http.MethodDelete}),
	)(router)

	// // initialize service
	service.InitializeOrder(logger, basicAuth, router, mariadbDB)

	logger.Infof("Application is running on port %s", port)
	logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), handler))
}

func getLogFormatter() *logrus.JSONFormatter {
	return &logrus.JSONFormatter{
		TimestampFormat: date.ISOFormat,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			s := strings.Split(f.Function, ".")
			funcname := s[len(s)-1]
			_, filename := path.Split(f.File)
			return funcname, filename
		},
	}
}
