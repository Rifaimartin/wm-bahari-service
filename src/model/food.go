package model

// Food is a struct
type Food struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Price     string `json:"price"`
	Qty       string `json:"qty"`
	CreatedAt string `json:"createdAt"`
	UpdatedAt string `json:"updatedAt"`
}
