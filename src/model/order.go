package model

// Order is a struct
type Order struct {
	ID        string   `json:"id" validate:"required,uuid_rfc4122"`
	Customer  Customer `json:"customer" validate:"required"`
	Food      Food     `json:"food" validate:"required"`
	CreatedAt string   `json:"createdAt"`
	UpdatedAt string   `json:"updatedAt"`
}
