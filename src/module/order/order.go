package issuer

import (
	"context"

	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/wrapper"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/model"
)

// MySQLCommandRepository is an interface
type MySQLCommandRepository interface {
	InsertOne(ctx context.Context, order *model.Order) (err error)
}

// MySQLQueryRepository is an interface
type MySQLQueryRepository interface {
	// for query
}

// CommandUsecase is an interface
type CommandUsecase interface {
	InsertOne(ctx context.Context, order *model.Order) wrapper.Result
}
