package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/wrapper"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/middleware"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/model"
	o "gitlab.com/Rifaimartin/wm-bahari-service/src/module/order"
)

// OrderHTTPHandler is a struct
type OrderHTTPHandler struct {
	Validate *validator.Validate
	OCUC     o.CommandUsecase
}

// NewOrderHTTPHandler is a constructor
func NewOrderHTTPHandler(
	router *mux.Router,
	auth *middleware.BasicAuth,
	ocuc o.CommandUsecase,
) {

	ohh := OrderHTTPHandler{
		OCUC: ocuc,
	}

	router.HandleFunc("", auth.Verify(ohh.PostOrder)).Methods(http.MethodPost)
}

// PostOrder is a function that handle incomming request to create banner135
func (ohh OrderHTTPHandler) PostOrder(w http.ResponseWriter, i *http.Request) {
	var result wrapper.Result
	var order model.Order

	ctx := i.Context()

	if err := json.NewDecoder(i.Body).Decode(&order); err != nil {
		result = wrapper.ErrorResult(wrapper.ErrUnprocessableEntitiy, err.Error(), wrapper.StatInvalidRequest)

		wrapper.ResponseError(w, &result)
		return
	}

	result = ohh.OCUC.InsertOne(ctx, &order)
	if result.Err != nil {
		wrapper.ResponseError(w, &result)
		return
	}

	wrapper.ResponseSuccess(w, http.StatusCreated, &result, "order is successfuly created")
	return
}
