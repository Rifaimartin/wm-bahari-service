package usecase

import (
	"context"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/wrapper"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/model"
	o "gitlab.com/Rifaimartin/wm-bahari-service/src/module/order"
)

type orderCommand struct {
	logger *logrus.Logger
	omcr   o.MySQLCommandRepository
}

// NewOrderCommand is a constructor
func NewOrderCommand(
	logger *logrus.Logger,
	omcr o.MySQLCommandRepository,
) o.CommandUsecase {
	return &orderCommand{
		logger: logger,
		omcr:   omcr,
	}
}

func (oc *orderCommand) InsertOne(ctx context.Context, order *model.Order) wrapper.Result {
	order.ID = uuid.New().String()

	err := oc.omcr.InsertOne(ctx, order)
	if err != nil {
		oc.logger.Error(err)
		return wrapper.ErrorResult(wrapper.ErrInternalServer, err.Error(), wrapper.StatUnexpectedError)
	}

	return wrapper.SuccessResult(nil, wrapper.StatOK)
}
