package repository

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/date"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/model"
	o "gitlab.com/Rifaimartin/wm-bahari-service/src/module/order"
)

type orderMySQLCommand struct {
	db          *sql.DB
	utcLocation *time.Location
}

// NewOrderMySQLCommand is a constructor
func NewOrderMySQLCommand(db *sql.DB) o.MySQLCommandRepository {
	utcLocation, _ := time.LoadLocation("UTC")
	return &orderMySQLCommand{
		db:          db,
		utcLocation: utcLocation,
	}
}

func (omc orderMySQLCommand) exec(ctx context.Context, orderCommand, customerCommand, foodCommand string, orderParams, customerParams, foodParams []interface{}) (execResult map[string]int64, err error) {
	execResult = make(map[string]int64, 0)

	tx, err := omc.db.BeginTx(ctx, nil)

	// preparing command
	orderCmdStmt, err := tx.PrepareContext(ctx, orderCommand)
	if err != nil {
		return
	}

	customerCmdStmt, err := tx.PrepareContext(ctx, customerCommand)
	if err != nil {
		return
	}

	foodCmdStmt, err := tx.PrepareContext(ctx, foodCommand)
	if err != nil {
		return
	}

	// command execution
	result, err := orderCmdStmt.ExecContext(ctx, orderParams...)
	if err != nil {
		fmt.Println("CHECK ERROR")
		return
	}

	execResult["orderLastInsertID"], _ = result.LastInsertId()

	result, err = customerCmdStmt.ExecContext(ctx, customerParams...)
	if err != nil {
		tx.Rollback()
		return
	}

	result, err = foodCmdStmt.ExecContext(ctx, customerParams...)
	if err != nil {
		tx.Rollback()
		return
	}

	return
}

func (omc orderMySQLCommand) InsertOne(ctx context.Context, order *model.Order) (err error) {
	orderCommand := `
		INSERT INTO order
		SET id = ?, id_customer = ?, id_food = ?, created_at = ?, updated_at = ?
	`

	customerCommand := `
		INSERT INTO customer
		SET id = ?, name = ?, phone_number = ?, email = ?, created_at = ?, updated_at = ?
	`

	foodCommand := `
		INSERT INTO food
		SET id = ?, name = ?, price = ?, qty = ?, created_at = ?, updated_at = ?
`
	createdAt, updatedAt := omc.toMySQLTypeAndValue(order)
	orderParams := []interface{}{
		order.ID, order.Customer.ID, order.Food.ID, createdAt, updatedAt,
	}

	fmt.Println(orderParams, "check order params")

	customerParams := []interface{}{
		order.Customer.ID, order.Customer.Name, order.Customer.PhoneNumber, order.Customer.Email,
		order.Customer.CreatedAt, order.Customer.UpdatedAt,
	}

	foodParams := []interface{}{
		order.Food.ID, order.Food.Name, order.Food.Price, order.Food.Qty,
		order.Food.CreatedAt, order.Food.UpdatedAt,
	}

	_, err = omc.exec(ctx, orderCommand, customerCommand, foodCommand, orderParams, customerParams, foodParams)

	return
}

func (omc orderMySQLCommand) toMySQLTypeAndValue(order *model.Order) (createdAt, updatedAt string) {
	asiaJakarta := "Asia/Jakarta"
	ct, _ := time.ParseInLocation(date.ISOFormat, order.CreatedAt, omc.utcLocation)
	ut, _ := time.ParseInLocation(date.ISOFormat, order.UpdatedAt, omc.utcLocation)

	createdAt, _ = date.ToMySQLDate(ct, asiaJakarta)
	updatedAt, _ = date.ToMySQLDate(ut, asiaJakarta)
	return
}
