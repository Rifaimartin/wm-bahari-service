package service

import (
	"database/sql"

	"gitlab.com/Rifaimartin/wm-bahari-service/src/middleware"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/module/order/handler"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/module/order/repository"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/module/order/usecase"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

// InitializeOrder will initialize and run order domain
func InitializeOrder(
	logger *logrus.Logger,
	auth *middleware.BasicAuth,
	router *mux.Router,
	db *sql.DB,
) {
	v1Router := router.PathPrefix("/wm-bahari/v1/order").Subrouter()

	imcr := repository.NewOrderMySQLCommand(db)

	icuc := usecase.NewOrderCommand(logger, imcr)

	handler.NewOrderHTTPHandler(v1Router, auth, icuc)
}
