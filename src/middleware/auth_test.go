package middleware_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Rifaimartin/wm-bahari-service/src/middleware"
)

func TestBasicAuth(t *testing.T) {
	basicAuth := middleware.BasicAuth{
		Username: "testusername",
		Password: "testpassword",
	}

	t.Run("should not be verified because of incorect username or password", func(t *testing.T) {
		mockHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
		handler := http.HandlerFunc(basicAuth.Verify(mockHandler))

		r := httptest.NewRequest(http.MethodGet, "/just/for/testing", nil)
		r.SetBasicAuth("wrong-username", "wrong-password")
		recorder := httptest.NewRecorder()
		handler.ServeHTTP(recorder, r)

		assert.Equal(t, http.StatusUnauthorized, recorder.Code)
	})

	t.Run("should not be verified because of invalid basic auth token", func(t *testing.T) {
		mockHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})
		handler := http.HandlerFunc(basicAuth.Verify(mockHandler))

		r := httptest.NewRequest(http.MethodGet, "/just/for/testing", nil)
		r.Header.Set("Authorization", "Bearer fake-token")
		recorder := httptest.NewRecorder()
		handler.ServeHTTP(recorder, r)

		assert.Equal(t, http.StatusUnauthorized, recorder.Code)
	})

	t.Run("should not be varified", func(t *testing.T) {
		mockHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			_, _, ok := r.BasicAuth()
			assert.Equal(t, true, ok)

			w.WriteHeader(http.StatusOK)
			io.WriteString(w, "Verified")
		})
		handler := http.HandlerFunc(basicAuth.Verify(mockHandler))

		r := httptest.NewRequest(http.MethodGet, "/just/for/testing", nil)
		r.SetBasicAuth("testusername", "testpassword")
		recorder := httptest.NewRecorder()
		handler.ServeHTTP(recorder, r)

		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, "Verified", recorder.Body.String())
	})
}
