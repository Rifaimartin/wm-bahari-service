package middleware

import (
	"net/http"

	"gitlab.com/Rifaimartin/wm-bahari-service/pkg/wrapper"
)

// BasicAuth is a property that contains request authorization
type BasicAuth struct {
	Username string
	Password string
}

// Verify returns next handler function when the request is authorized and returns http error unauthorized instead
func (ba *BasicAuth) Verify(next http.HandlerFunc) http.HandlerFunc {
	result := new(wrapper.Result)
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			result.Err = wrapper.ErrUnauthorized
			result.ErrMessage = "Token is invalid"
			result.Status = wrapper.StatInvalidRequest
			wrapper.ResponseError(w, result)
			return
		}

		if !(username == ba.Username && password == ba.Password) {
			result.Err = wrapper.ErrUnauthorized
			result.ErrMessage = "Incorrect username or password"
			result.Status = wrapper.StatInvalidUser
			wrapper.ResponseError(w, result)
			return
		}

		next(w, r)
	})
}
